#include <iostream>
#include <cstdlib>
#include <ctime>
#include "strategy1.h"
#include "strategy2.h"

using namespace std;

string who_won_overall(int greedy_wins, int random_wins) {
if (greedy_wins > random_wins) {
return "Greedy algorithm won overall!";
} else if (greedy_wins < random_wins) {
return "Random choice method won overall!";
} else {
return "It's a tie overall!";
}
}

int main() {
srand(time(0));

int greedy_wins = 0;
int random_wins = 0;
int ties = 0;

int num_iterations;
cout << "Enter the number of iterations: ";
cin >> num_iterations;

for (int i = 0; i < num_iterations; i++) {
    char greedy_choice = get_greedy_choice();
    char random_choice = get_random_choice();
    string winner = determine_winner(greedy_choice, random_choice);
    if (winner == "greedy") {
        greedy_wins++;
    } else if (winner == "random") {
        random_wins++;
    } else {
        ties++;
    }
}

cout << "Greedy algorithm won " << greedy_wins << " times." << endl;
cout << "Random choice method won " << random_wins << " times." << endl;
cout << "There were " << ties << " ties." << endl;

cout << who_won_overall(greedy_wins, random_wins) << endl;

return 0;
}