#include "strategy1.h"
#include <cstdlib>
#include <ctime>

char get_greedy_choice() {
    srand(time(0));
    int choice = rand() % 3;
    switch (choice) {
        case 0:
            return 'r';
        case 1:
            return 's';
        case 2:
            return 'p';
        default:
            return 'r';
    }
}

    string determine_winner(char player_choice, char opponent_choice) {
    if (player_choice == opponent_choice) {
        return "tie";
    } else if (player_choice == 'r') {
        if (opponent_choice == 's') {
            return "greedy";
        } else {
            return "opponent";
        }
    } else if (player_choice == 's') {
        if (opponent_choice == 'p') {
            return "greedy";
        } else {
            return "opponent";
        }
    } else if (player_choice == 'p') {
        if (opponent_choice == 'r') {
            return "greedy";
        } else {
            return "opponent";
        }
    } else {
        return "opponent";
    }
}