#include "strategy2.h"
#include <cstdlib>

char get_random_choice() {
    // Randomly selects a choice among "rock", "scissors", or "paper"
    int random_num = rand() % 3;
    if (random_num == 0) {
        return 'r';
    } else if (random_num == 1) {
        return 's';
    } else {
        return 'p';
    }
}