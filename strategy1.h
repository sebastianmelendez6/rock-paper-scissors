#pragma once

using namespace std;
#include <string>

char get_greedy_choice();
string determine_winner(char player_choice, char opponent_choice);
